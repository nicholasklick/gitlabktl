package runtime

import (
	"github.com/stretchr/testify/mock"
)

type MockClient struct {
	mock.Mock
}

func (client *MockClient) ReadFile(path string) string {
	args := client.Called(path)

	return args.String(0)
}
