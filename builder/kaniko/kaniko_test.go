package kaniko

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

func newKaniko() Kaniko {
	return Kaniko{
		Dockerpath:   "Dockerfile.tmp",
		Dockerfile:   "FROM scratch",
		Workspace:    "build/",
		Destinations: []string{"my-image:1", "my-image:2"},
		Registry: registry.Registry{
			Host: "registry",
			Credentials: registry.Credentials{
				Username: "my-user",
				Password: "my-pass",
			},
		},
	}
}

func TestExecutorCommand(t *testing.T) {
	kaniko := newKaniko()

	command := kaniko.commandToString()

	assert.Equal(t, command,
		strings.Join([]string{"/kaniko/executor",
			"--context build/",
			"--dockerfile Dockerfile.tmp",
			"--destination my-image:1",
			"--destination my-image:2",
			"--cleanup"}, " "),
	)
}

func TestDryRun(t *testing.T) {
	kaniko := newKaniko()
	summary, err := kaniko.DryRun()
	assert.NoError(t, err)

	assert.Contains(t, summary, "/kaniko/executor")
	assert.Contains(t, summary, "FROM scratch")
}

func TestNewWriteDockerfile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		kaniko.writeDockerfile()
		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "FROM scratch", string(output))
	})
}

func TestExistingWriteDockerfile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		err := fs.WriteFile(kaniko.Dockerpath, []byte("stub"), 664)
		require.NoError(t, err)

		kaniko.writeDockerfile()
		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "stub", string(output))
	})
}

func TestNewWriteAuthFile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		kaniko.writeAuthFile()
		output, err := fs.ReadFile(authfile)
		require.NoError(t, err)

		assert.Contains(t, string(output), "auths")
	})
}

func TestExistingWriteAuthFile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		err := fs.WriteFile(authfile, []byte("stub"), 664)
		require.NoError(t, err)

		kaniko.writeAuthFile()
		output, err := fs.ReadFile(authfile)
		require.NoError(t, err)

		assert.Contains(t, string(output), "auths")
	})
}

func TestDockerfileWrite(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()

		kaniko.writeDockerfile()
		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "FROM scratch", string(output))
	})
}

func TestNoDockerfileContents(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := Kaniko{
			Dockerpath:   "Dockerfile.tmp",
			Workspace:    "build/",
			Destinations: []string{"my-image"},
		}

		err := fs.WriteFile(kaniko.Dockerpath, []byte("FROM ruby"), 664)
		require.NoError(t, err)

		kaniko.writeDockerfile()

		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "FROM ruby", string(output))
	})
}

func TestNoDockerfile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := Kaniko{
			Dockerpath:   "Dockerfile.tmp",
			Workspace:    "build/",
			Destinations: []string{"my-image"},
		}

		assert.Panics(t, func() { kaniko.writeDockerfile() })
	})
}
