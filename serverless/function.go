package serverless

import (
	"gitlab.com/gitlab-org/gitlabktl/runtime"

	tmfile "github.com/triggermesh/tm/pkg/file"
)

type Function struct {
	Name     string
	Service  string
	Manifest tmfile.Function
	Runtime  runtime.Details
}

func (function Function) ToRuntime() runtime.Runtime {
	return runtime.New(function.Runtime)
}
