package registry

import (
	"github.com/sirupsen/logrus"
	"github.com/triggermesh/tm/pkg/client"
	"github.com/triggermesh/tm/pkg/resources/credential"
)

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (registry Registry) DeployCredentials(clientset *client.ConfigSet) error {
	logrus.Info("deploying registry credentials")

	credentials := credential.RegistryCreds{
		Name:      "gitlab-registry",
		Namespace: client.Namespace,
		Host:      registry.Host,
		Username:  registry.Username,
		Password:  registry.Password,
		Pull:      true,
	}

	return credentials.CreateRegistryCreds(clientset)
}
