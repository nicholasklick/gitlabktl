package registry

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
)

func TestAuthFileContents(t *testing.T) {
	registry := Registry{
		Host: "my-registry.test",
		Credentials: Credentials{
			Username: "my-user",
			Password: "my-pass",
		},
	}

	contents, err := registry.ToAuthFileContents()

	require.NoError(t, err)
	assert.Equal(t, `{"auths":{"my-registry.test":{"username":"my-user","password":"my-pass"}}}`, contents)
}

func TestNewWithPushAccess(t *testing.T) {
	config := env.Stubs{
		"CI_REGISTRY":          "registry.test",
		"CI_REGISTRY_USER":     "my-user",
		"CI_REGISTRY_PASSWORD": "my-password",
	}

	env.WithStubbedEnv(config, func() {
		registry := NewWithPushAccess()

		assert.Equal(t, "registry.test", registry.Host)
		assert.Equal(t, "my-user", registry.Username)
		assert.Equal(t, "my-password", registry.Password)
	})
}

func TestNewWithPullAccess(t *testing.T) {
	config := env.Stubs{
		"CI_REGISTRY":        "registry.test",
		"CI_REGISTRY_IMAGE":  "registry.test/repository",
		"CI_DEPLOY_USER":     "my-user",
		"CI_DEPLOY_PASSWORD": "my-pass",
	}

	env.WithStubbedEnv(config, func() {
		registry := NewWithPullAccess()

		assert.Equal(t, "registry.test", registry.Host)
		assert.Equal(t, "my-user", registry.Username)
		assert.Equal(t, "my-pass", registry.Password)
	})
}

func TestMissingRegistryEnv(t *testing.T) {
	env.WithStubbedEnv(env.Stubs{}, func() {
		assert.Panics(t, func() { NewWithPushAccess() })
	})
}

func TestMissingDeployToken(t *testing.T) {
	env.WithStubbedEnv(env.Stubs{}, func() {
		assert.Panics(t, func() { NewWithPullAccess() })
	})
}

func TestDefaultRepository(t *testing.T) {
	env.WithStubbedEnv(env.Stubs{"CI_REGISTRY_IMAGE": "registry.test/repository"}, func() {
		assert.Equal(t, "registry.test/repository", DefaultRepository())
	})
}

func TestDefaultRepositoryNestedImage(t *testing.T) {
	env.WithStubbedEnv(env.Stubs{"CI_REGISTRY": "registry.test"}, func() {
		assert.Equal(t, IsDefaultRegistry("registry.test/repository"), true)
	})
}
