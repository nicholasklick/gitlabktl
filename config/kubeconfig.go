package config

import (
	"path/filepath"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

type KubeConfig struct {
	Path string
}

func (config KubeConfig) FilePath() string {
	if len(config.Path) > 0 {
		path, _ := filepath.Abs(config.Path)
		return path
	}

	return env.Getenv("KUBECONFIG")
}

func (config KubeConfig) IsMissing() bool {
	exists, err := fs.Exists(config.FilePath())
	if err != nil {
		logrus.WithError(err).Fatal("could not verify kubeconfig file existence")
	}

	return !exists
}

func NewKubeConfigPath(path string) string {
	kubeconfig := KubeConfig{Path: path}

	if kubeconfig.IsMissing() {
		logrus.WithField("kubeconfig", kubeconfig.FilePath()).
			Fatal("kubeconfig file missing")
	}

	return kubeconfig.FilePath()
}
