package config

import (
	"fmt"
	"os"
	"path"

	"github.com/sirupsen/logrus"
	tmfile "github.com/triggermesh/tm/pkg/file"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/registry"
	"gitlab.com/gitlab-org/gitlabktl/runtime"
	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

type Serverless struct {
	tmfile.Definition `yaml:",inline"`
}

const ServerlessFile = "serverless.yml"

func NewServerlessConfig() Serverless {
	return parseServerlessYAML(ServerlessFile)
}

func parseServerlessYAML(path string) Serverless {
	tmfile.Aos = fs.GetFs()

	definition, err := tmfile.ParseManifest(path)

	if err != nil {
		logrus.WithField("path", path).WithError(err).Fatal("could not parse manifest")
	}

	manifest := Serverless{Definition: definition}

	if len(manifest.Provider.Registry) == 0 {
		manifest.Provider.Registry = registry.DefaultRepository()
	}

	return manifest
}

func (manifest Serverless) BaseDirectory() string {
	pwd, err := os.Getwd()
	if err != nil {
		logrus.WithError(err).Fatal("could not determine current working directory")
	}

	return pwd
}

func (manifest Serverless) ToFunctions() (functions []serverless.Function) {
	baseDirectory := manifest.BaseDirectory()

	for name, function := range manifest.Functions {
		service := fmt.Sprintf("%s-%s", manifest.Service, name)

		newFunction := serverless.Function{
			Name:     name,
			Service:  service,
			Manifest: function,
			Runtime: runtime.Details{
				FunctionName:    name,
				FunctionHandler: function.Handler,
				RuntimeAddress:  function.Runtime,
				CodeDirectory:   path.Join(baseDirectory, function.Source),
				ResultingImage:  path.Join(manifest.Provider.Registry, service),
			},
		}

		functions = append(functions, newFunction)
	}

	return functions
}
