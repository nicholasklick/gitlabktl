package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"

	mocks "gitlab.com/gitlab-org/gitlabktl/mocks/runtime"
)

func TestReadFile(t *testing.T) {
	client := new(mocks.MockClient)
	location := Location{
		Address:   "https://gitlab.com/grzesiek/some-runtime",
		Reference: "v0.1alpha1",
	}

	repository := Repository{
		Client:   client,
		Location: &location,
	}

	client.On("ReadFile", "template").Return("FROM scratch").Once()

	assert.Equal(t, "FROM scratch", repository.ReadFile("template"))
	client.AssertExpectations(t)
}
