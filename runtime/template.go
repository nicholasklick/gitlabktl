package runtime

import (
	"bytes"

	"github.com/sirupsen/logrus"
	engine "text/template"
)

type Template struct {
	*Repository
	Filename string
	runtime  Details
}

func NewTemplate(details Details) *Template {
	return &Template{
		runtime:    details,
		Filename:   "Dockerfile.template",
		Repository: NewRepository(details.RuntimeAddress),
	}
}

func (template *Template) Dockerfile() string {
	logrus.WithField("filename", template.Filename).Info("Parsing the template")

	contents := template.ReadFile(template.Filename)
	parser := engine.Must(engine.New("Dockerfile").Parse(contents))
	buffer := new(bytes.Buffer)
	err := parser.Execute(buffer, template.runtime)

	if err != nil {
		logrus.WithError(err).Fatal("could not execute the template")
	}

	return buffer.String()
}
