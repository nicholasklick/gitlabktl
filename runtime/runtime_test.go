package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
	mocks "gitlab.com/gitlab-org/gitlabktl/mocks/runtime"
)

func TestRuntimeDockerfile(t *testing.T) {
	details := Details{
		FunctionName:   "my-function",
		RuntimeAddress: "some/address",
	}

	client := new(mocks.MockClient)
	client.On("ReadFile", "Dockerfile.template").Return("FROM scratch").Once()
	defer client.AssertExpectations(t)

	newTemplate = func(details Details) *Template {
		return &Template{
			runtime:  details,
			Filename: "Dockerfile.template",
			Repository: &Repository{
				Location: &Location{
					Address:   details.RuntimeAddress,
					Reference: "v1alpha1",
				},
				Client: client,
			},
		}
	}

	runtime := New(details)

	assert.Equal(t, "FROM scratch", runtime.Dockerfile())
}

func TestRuntimeSlug(t *testing.T) {
	runtime := Runtime{
		Details: Details{
			FunctionName: "my-ąłę-func 1",
		},
	}

	assert.Equal(t, "my-ale-func-1", runtime.Slug())
}
