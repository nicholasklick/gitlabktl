# GitLab Knative tool

`gitlabktl` is a tool that makes it easier to build and deploy your serverless
applications and functions to Knative using GitLab CI/CD.

It uses:

* [Kaniko](https://github.com/GoogleContainerTools/kaniko) to build functions
* GitLab Container Registry / custom registry to store images
* [tm](https://github.com/triggermesh/tm) tool to deploy services


## Usage

See `gitlabktl --help`

1. Bulding a function runtime

    ```
    gitlabktl function build --help
    ```
    
1. Build functions defined in a serverless configuration file

    ```
    gitlabktl serverless build --help
    ```

1. Deploy functions defined in a serverless configuration file

    ```
    gitlabktl serverless deploy --help
    ```
    
1. Build a serverless application using a `Dockerfile`

    ```
    gitlabktl app build --help
    ```
    
## State

Alpha

## License

MIT
