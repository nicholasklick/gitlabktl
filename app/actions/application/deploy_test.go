package application

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
)

func TestDefaultSourceImage(t *testing.T) {
	action := deployAction{}

	env.WithStubbedEnv(env.Stubs{"CI_REGISTRY_IMAGE": "my.registry/project"}, func() {
		assert.Equal(t, "my.registry/project:latest", action.applicationSource())
	})
}

func TestAppDeployDryRun(t *testing.T) {
	buffer := new(bytes.Buffer)
	output = buffer
	defer func() { output = os.Stdout }()

	action := deployAction{
		Name:    "my-app",
		Image:   "my.registry/my/image",
		Config:  "../../../config/testdata/kubeconfig.json",
		DryRun:  true,
		Secrets: []string{"my-secret"},
		Envs:    []string{"my-env"},
	}

	err := action.Execute(new(commands.Context))
	require.NoError(t, err)
	message := buffer.String()

	assert.Contains(t, message, `"kind": "Service"`)
	assert.Contains(t, message, `"apiVersion": "serving.knative.dev/v1alpha1"`)
	assert.Contains(t, message, `"image": "my.registry/my/image:latest"`)
	assert.Contains(t, message, `"namespace": "testNamespace"`)
	assert.Contains(t, message, `"name": "my-app"`)
	assert.Contains(t, message, `"name": "my-secret"`)
}
