package application

import (
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/builder"
	"gitlab.com/gitlab-org/gitlabktl/image"
)

type buildAction struct {
	Dockerfile string   `long:"dockerfile" env:"GITLAB_APP_DOCKERFILE" description:"Dockerfile path"`
	Directory  string   `long:"directory" env:"GITLAB_APP_DIRECTORY" description:"Directory with code"`
	Image      string   `long:"image" env:"GITLAB_APP_IMAGE" description:"Resulting image repository"`
	Tag        string   `long:"tag" env:"GITLAB_APP_TAG" description:"Resulting image tag"`
	Aliases    []string `long:"alias" env:"GITLAB_APP_TAG_ALIAS" description:"Additional image tag"`
	Username   string   `long:"registry-username" env:"GITLAB_REGISTRY_USERNAME" description:"Registry username"`
	Password   string   `long:"registry-password" env:"GITLAB_REGISTRY_PASSWORD" description:"Registry password"`
	DryRun     bool     `short:"t" long:"dry-run" env:"GITLAB_APP_DRYRUN" description:"Dry run only"`
}

func (action *buildAction) Execute(context *commands.Context) error {
	builder := action.Builder()

	if action.DryRun {
		_, err := builder.DryRun()

		return err
	}

	return builder.Build(context.Ctx)
}

func (action *buildAction) Builder() builder.Builder {
	return builder.NewFromRequest(builder.Request{
		Dockerfile: action.SourceDockerfile(),
		Directory:  action.SourceContext(),
		Image:      action.ToImage(),
	})
}

func (action *buildAction) SourceDockerfile() string {
	if len(action.Dockerfile) > 0 {
		return action.Dockerfile
	}

	return "Dockerfile"
}

func (action *buildAction) SourceContext() string {
	pwd, err := os.Getwd()
	if err != nil {
		logrus.WithError(err).Fatal("could not read current directory")
	}

	if len(action.Directory) == 0 {
		return pwd
	}

	context, err := filepath.Abs(action.Directory)
	if err != nil {
		logrus.WithError(err).Fatal("could not calculate absolute context path")
	}

	return context
}

func (action *buildAction) ToImage() image.Image {
	return image.Image{
		Repository: action.Image,
		Tag:        action.Tag,
		Aliases:    action.Aliases,
		Username:   action.Username,
		Password:   action.Password,
	}
}

func newBuildCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "application/build",
			Config: commands.Config{
				Name:        "build",
				Aliases:     []string{},
				Usage:       "Build your serverless application",
				Description: "This commands builds an image using a Dockerfile",
			},
		},
		Handler: new(buildAction),
	}
}

func RegisterBuildCommand(register *commands.Register) {
	register.RegisterCommand(newBuildCommand())
}
