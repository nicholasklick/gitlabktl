package application

import (
	"fmt"
	"io"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/triggermesh/tm/pkg/client"
	"github.com/triggermesh/tm/pkg/resources/service"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/config"
	"gitlab.com/gitlab-org/gitlabktl/image"
)

var output io.Writer = os.Stdout

type deployAction struct {
	Name     string   `long:"name" env:"GITLAB_APP_NAME" description:"Application name"`
	Image    string   `long:"image" env:"GITLAB_APP_IMAGE" description:"Source image repository"`
	Tag      string   `long:"tag" env:"GITLAB_APP_TAG" description:"Source image tag"`
	Username string   `long:"registry-username" env:"GITLAB_REGISTRY_USERNAME" description:"Registry username"`
	Password string   `long:"registry-password" env:"GITLAB_REGISTRY_PASSWORD" description:"Registry password"`
	Config   string   `short:"c" long:"kubeconfig" env:"GITLAB_KUBECONFIG_FILE" description:"Path to kubeconfig"`
	DryRun   bool     `short:"t" long:"dry-run" env:"GITLAB_APP_DRYRUN" description:"Dry run only"`
	Secrets  []string `long:"secret" description:"Kubernetes secrets"`
	Envs     []string `long:"env" description:"Application environment variables"`
}

func (action *deployAction) Execute(context *commands.Context) error {
	client.Namespace = action.namespace()
	client.Dry = action.DryRun
	client.Wait = true

	config := config.NewKubeConfigPath(action.Config)
	clientset, err := client.NewClient(config)

	if err != nil {
		logrus.WithError(err).Fatal("could not load kubernetes config")
	}

	err = action.deployCredentials(&clientset)

	if err != nil {
		logrus.WithError(err).Fatal("could not deploy registry credentials")
	}

	service := &service.Service{
		Namespace:  client.Namespace,
		Registry:   client.Registry,
		Source:     action.applicationSource(),
		Name:       action.applicationName(),
		EnvSecrets: action.Secrets,
		Env:        action.Envs,
	}

	message, err := service.Deploy(&clientset)

	if err == nil {
		fmt.Fprint(output, message)
	}

	return err
}

func (action *deployAction) ToImage() image.Image {
	return image.Image{
		Repository: action.Image,
		Tag:        action.Tag,
		Username:   action.Username,
		Password:   action.Password,
	}
}

func (action *deployAction) namespace() string {
	return env.Getenv("KUBE_NAMESPACE")
}

func (action *deployAction) applicationSource() string {
	return action.ToImage().ToString()
}

func (action *deployAction) applicationName() string {
	if len(action.Name) > 0 {
		return action.Name
	}

	return env.Getenv("CI_PROJECT_NAME")
}

func (action *deployAction) deployCredentials(clientset *client.ConfigSet) error {
	if action.DryRun {
		return nil
	}

	registry := action.ToImage().PullRegistry()

	return registry.DeployCredentials(clientset)
}

func newDeployCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "application/deploy",
			Config: commands.Config{
				Name:        "deploy",
				Aliases:     []string{},
				Usage:       "Deploy your serverless application",
				Description: "This commands deploys an image of you serverless application",
			},
		},
		Handler: new(deployAction),
	}
}

func RegisterDeployCommand(register *commands.Register) {
	register.RegisterCommand(newDeployCommand())
}
