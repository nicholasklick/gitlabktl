package serverless

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/triggermesh/tm/pkg/resources/service"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
)

func TestDryRunDeployment(t *testing.T) {
	buffer := new(bytes.Buffer)
	service.Output = buffer
	defer func() { service.Output = os.Stdout }()

	action := DeployAction{
		DryRun:     true,
		ConfigFile: "../../../config/testdata/serverless.yml",
		KubeConfig: "../../../config/testdata/kubeconfig.json",
	}

	err := action.Execute(new(commands.Context))
	require.NoError(t, err)
	output := buffer.String()

	assert.Contains(t, output, `"kind": "Service"`)
	assert.Contains(t, output, `"apiVersion": "serving.knative.dev/v1alpha1"`)
	assert.Contains(t, output, `"image": "registry.gitlab.com/my-project/my-functions-echo"`)
}
