package serverless

import (
	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/builder"
	"gitlab.com/gitlab-org/gitlabktl/config"
)

// BuildAction struct is going to be populated with CLI invocation arguments.
type BuildAction struct {
	DryRun bool `short:"t" long:"dry-run" env:"GITLAB_DRYRUN" description:"Dry run only"`
}

func (action *BuildAction) Execute(context *commands.Context) error {
	functions := config.NewServerlessConfig().ToFunctions()

	for _, function := range functions {
		executor := builder.NewFromRuntime(function.ToRuntime())

		var err error
		if action.DryRun {
			_, err = executor.DryRun()
		} else {
			err = executor.Build(context.Ctx)
		}

		if err != nil {
			return err
		}
	}

	return nil
}

func newBuildCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "serverless/build",
			Config: commands.Config{
				Name:        "build",
				Aliases:     []string{},
				Usage:       "Build your serverless services",
				Description: "This commands reads serverless.yml and builds your images",
			},
		},
		Handler: new(BuildAction),
	}
}

func RegisterBuildCommand(register *commands.Register) {
	register.RegisterCommand(newBuildCommand())
}
