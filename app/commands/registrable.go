package commands

import (
	"strings"

	"github.com/urfave/cli"
)

// Command configuration, using `urfave/cli`
type Config cli.Command

// Executable is an interface for an executable action
type Executable interface {
	Execute(context *Context) error
}

// Category is a group of executable CLI commands
type Category struct {
	Registrable
}

// Command is an instance of executable CLI invocation
type Command struct {
	Registrable
	Handler Executable
}

// Registrable is an entity that can be registered in commands graph / tree
type Registrable struct {
	Path string
	Config
}

func (cmd Command) toActionFunc() cli.ActionFunc {
	return func(ctx *cli.Context) error {
		return cmd.Handler.Execute(newContext(ctx))
	}
}

func (cmd Registrable) parentPath() string {
	path := strings.Split(cmd.Path, "/")
	path = path[:len(path)-1]

	return strings.Join(path, "/")
}

func (cmd Registrable) root() bool {
	return len(strings.Split(cmd.Path, "/")) == 1
}
