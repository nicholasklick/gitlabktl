package app

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/gitlabktl/app/actions/application"
	"gitlab.com/gitlab-org/gitlabktl/app/actions/function"
	"gitlab.com/gitlab-org/gitlabktl/app/actions/serverless"
	"gitlab.com/gitlab-org/gitlabktl/app/commands"
)

type App struct {
	cli      *cli.App
	register *commands.Register
}

func (app *App) Run(args []string) error {
	app.cli.Commands = app.register.GetCommands()

	if len(app.cli.Commands) < 1 {
		logrus.Fatal("no commands registered")
	}

	return app.cli.Run(args)
}

func (app *App) RegisterCommands() {
	function.RegisterCategory(app.register)
	function.RegisterBuildCommand(app.register)
	serverless.RegisterCategory(app.register)
	serverless.RegisterBuildCommand(app.register)
	serverless.RegisterDeployCommand(app.register)
	application.RegisterCategory(app.register)
	application.RegisterBuildCommand(app.register)
	application.RegisterDeployCommand(app.register)
}

func NewApp() *App {
	cli := cli.NewApp()
	cli.Name = "gitlabktl"
	cli.Usage = "Manage your serverless Knative / Kubernetes resources"
	cli.UsageText = "gitlabktl command [subcommand] [arguments...]"
	cli.Version = "0.3.0"
	register := commands.NewRegister()

	return &App{cli: cli, register: register}
}
