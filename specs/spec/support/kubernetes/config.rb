module Kubernetes
  class Config
    attr_reader :namespace

    def initialize(kubeconfig)
      raise ArgumentError if kubeconfig.to_s.empty?
      raise ArgumentError unless File.exists?(kubeconfig)

      @config = Kubeclient::Config.read(kubeconfig)
    end

    def context
      @config.context
    end

    def namespace
      context.namespace
    end

    def knative_endpoint
      [context.api_endpoint, 'apis/serving.knative.dev'].join('/')
    end

    def to_options
      { ssl_options: context.ssl_options,
        auth_options: context.auth_options }
    end

    def self.build!
      unless ENV['KUBECONFIG']
        raise ArgumentError, 'KUBECONFIG env is required!'
      end

      new(ENV['KUBECONFIG'])
    end
  end
end
