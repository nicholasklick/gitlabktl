app = Proc.new do |env|
  [200, { 'Content-Type' => 'text/plain' },
   StringIO.new("Hello World! #{File.read('uuid.txt')}")]
end

run app
